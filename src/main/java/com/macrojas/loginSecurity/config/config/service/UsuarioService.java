package com.macrojas.loginSecurity.config.config.service;

import java.util.List;
import com.macrojas.loginSecurity.config.config.entity.User;


public interface UsuarioService {
	
	public abstract List<User> listAllUsers();
	public abstract User addUsers(User user);

}
