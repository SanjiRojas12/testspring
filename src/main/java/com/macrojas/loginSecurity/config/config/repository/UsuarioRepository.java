package com.macrojas.loginSecurity.config.config.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.macrojas.loginSecurity.config.config.entity.User;

@Repository("usuariorepository")
public interface UsuarioRepository extends JpaRepository<User, Serializable >{
	
}
