package com.macrojas.loginSecurity.config.config.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.macrojas.loginSecurity.config.config.entity.User;

@Repository
public interface UserApiRepository extends JpaRepository<User, Long> {
	 
}