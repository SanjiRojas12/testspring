package com.macrojas.loginSecurity.config.config.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.macrojas.loginSecurity.config.config.entity.User;
import com.macrojas.loginSecurity.config.config.repository.UsuarioRepository;
import com.macrojas.loginSecurity.config.config.service.UsuarioService;

@Service("usuarioservice")
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	@Qualifier("usuariorepository")
	private UsuarioRepository usuarioRepository;

	@Override
	public List<User> listAllUsers() {

		return usuarioRepository.findAll();
	}

	@Override
	public User addUsers(User user) {

		return usuarioRepository.save(user);
	}
}
