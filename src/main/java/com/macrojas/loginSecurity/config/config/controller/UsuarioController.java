package com.macrojas.loginSecurity.config.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.macrojas.loginSecurity.config.config.entity.User;
import com.macrojas.loginSecurity.config.config.service.UsuarioService;

@Controller
@RequestMapping("/user")
public class UsuarioController {
	
	@Autowired
	@Qualifier("usuarioservice")
	private UsuarioService usuarioService;
	
	@GetMapping("/list")
	public ModelAndView listAllUsers() {
		ModelAndView view = new ModelAndView("list");
		view.addObject("usuarios", usuarioService.listAllUsers());
		view.addObject("user", new User());
		return view;
	}
	
	@PostMapping("/addusers")
	public String addUsers(@ModelAttribute(name="users") User user) {
		
		
		User newUser = new User();
		newUser.setName(user.getName());
		newUser.setUsername(user.getUsername());
		newUser.setEnabled(true);
		
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(4);
		String Paswword = bCryptPasswordEncoder.encode(user.getPassword());
		
		newUser.setPassword(Paswword);
		
		usuarioService.addUsers(newUser);
		return "redirect:/user/list";
	}

}
